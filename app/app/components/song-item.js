import Ember from 'ember';

const {
  inject: { service }
} = Ember;

export default Ember.Component.extend({
  tagName: '',
  player: service(),
  actions: {
    doPlay() {
      this.get('player').setSong(this.get('model'))
      this.get('player').play();
    },
    doPause() {
      this.get('player').pause();
    }
  }
});
