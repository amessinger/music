import Ember from 'ember';

const {
  computed,
  inject: { service }
} = Ember;

export default Ember.Component.extend({
  tagName: 'div',
  classNames: ['columns', 'is-multiline', 'is-mobile'],
  player: service()
});
