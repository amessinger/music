import Ember from 'ember';

const {
  computed,
  inject: { service }
} = Ember;

export default Ember.Component.extend({
  classNames: ['box'],
  player: service(),
  actions: {
    doPlay() {
      this.get('player').play();
    },
    doPause() {
      this.get('player').pause();
    }
  }
});
