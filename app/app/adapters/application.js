import Ember from 'ember';
import DS from 'ember-data';
import config from '../config/environment';
import AdapterFetch from 'ember-fetch/mixins/adapter-fetch';

const {
  computed,
  inject: { service }
} = Ember;

export default DS.RESTAdapter.extend(AdapterFetch, {
  fastboot: service(),
  host: computed('fastboot.isFastBoot', function() {
    return this.get('fastboot.isFastBoot') ? config.fastboot.apiHost : config.APP.apiHost;
  }),
  namespace: 'api'
});
