import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  album: DS.attr('string'),
  artist: DS.attr('string'),
  path: DS.attr('string')
});
