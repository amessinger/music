import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    doSelectSong(song) {
      this.set('selectedSong', song);
    }
  }
});
