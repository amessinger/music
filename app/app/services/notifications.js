import Ember from 'ember';

const {
  computed,
  Service
} = Ember;

function defaultOnClick() {
  window.focus();
}

function spawnNotification(title, body, icon, onClick = defaultOnClick) {
  let options = {
    body,
    icon
  };
  let notification = new Notification(title, options);
  notification.onclick = onClick;
  return notification;
}

export default Service.extend({
  isAvailable: computed(function() {
    return !('Notification' in window);
  }),
  isEnabled: computed(function() {
    return Notification.permission === 'granted';
  }),
  tryToEnable() {
    Notification.requestPermission((permission)=> {
      if (permission === 'granted') {
        this.set('isEnabled', true);
      }
    });
  },
  new(/* see spawnNotification for arguments */) {
    if (this.get('isAvailable')) {
      return;
    }

    if (!this.get('isEnabled')) {
      this.tryToEnable();
    }

    if (this.get('isEnabled')) {
      return spawnNotification(...arguments);
    } else {
      return;
    }
  }
});
