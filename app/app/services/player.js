import Ember from 'ember';
import config from '../config/environment';

const {
  inject: { service },
  Service
} = Ember;

export default Service.extend({
  fastboot: service(),
  notifications: service(),
  init() {
    this._super(...arguments);
    let isFastBoot = this.get('fastboot.isFastBoot');
    if (!isFastBoot) {
      this.set('audio', new Audio());
    }
  },
  setSong(song) {
    this.set('playingSong', song);
    this.get('audio').src = `${config.APP.apiHost}${song.get('path')}`;
    this.get('notifications').new(
      song.get('title'),
      `${song.get('album')} - ${song.get('artist')}`,
      'http://bulma.io/images/placeholders/128x128.png'
    );
    return this;
  },
  play() {
    this.set('isPlaying', true);
    return this.get('audio').play();
  },
  pause() {
    this.set('isPlaying', false);
    return this.get('audio').pause();
  }
});
