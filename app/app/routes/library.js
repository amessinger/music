import Ember from 'ember';
import fetch from 'ember-fetch/ajax';

export default Ember.Route.extend({
  model() {
    // return this.get('store').findAll('song');
    let modelName = 'song';
    let url = this.get('store').adapterFor(modelName).urlForFindAll(modelName);
    return fetch(url).then((response)=> response.map((song)=> this.get('store').createRecord(modelName, song)));
  }
});
