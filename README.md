# Music

## Description

A music service featuring :

- a web GUI
- a REST API
- a storage backend
- an authentication system (ROADMAP)
- https and http2 for WAN traffic

## Prerequisites

docker, docker-compose

https://docs.docker.com/compose/install/

## Setup

Edit [`.env`](.env) or set `STORAGE_FOLDER` environment variable to mount your music library.

## Use

`docker-compose up` then visit http://localhost.

Notes : You'll be redirected to the `https` server, upgraded to `http2` and warned about the SSL Certificate being selg-signed.

If you happen to have a `song.flac` file at the root for your `STORAGE_FOLDER`, then you'll be able to play it from your browser. Yay!

## Architecture

```
WORKER           (mutagen)                          FILER              (nginx)
+------------------------+                          +------------------------+
|                        |                          |                        |
|    reads               |                          |    exposes             |    :80
|    "/media/storage/" & +--------------+           |    "/media/storage/"   +-----------+
|    updates library     |              |           |                        |           |
|                        |              |           |                        |           |
+------------------------+              |           +------------------------+           |
                                        |                                                |
                                        |                                                |
                                        |                                                |
DATABASE         (mongodb)              |           API          (http+server)           |            PROXY     (nginx, openssl)
+------------------------+              |           +------------------------+           |            +------------------------+
|                        |              |           |                        |           +------------+ /*  <---->    /filer/* |
|    stores library      |    :27017    |           |    exposes             |    :80                 |                        |   :(80+443)
|                        +--------------+-----------+    a RESTFUL service   +------------------------+ /*  <---->      /api/* | <-----------> WAN
|                        |                          |                        |                        |                        |
|                        |                          |                        |           +------------+ /*  <---->          /* |
+------------------------+                          +------------------------+           |            +------------------------+
                                                                                         |
                                                                                         |
                                                                                         |
                                                    APP                (nginx)           |
                                                    +------------------------+           |
                                                    |                        |           |
                                                    |    exposes             |    :80    |
                                                    |    index.html and      +-----------+
                                                    |    webapp assets       |
                                                    |                        |
                                                    +------------------------+
```
