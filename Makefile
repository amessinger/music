namespace=$(dirname $(PWD))

start-dev:
	docker-compose up

start:
	docker-compose up -d

stop:
	docker-compose stop

populate:
	docker run $(namespace)_worker sh -c 'source ../venv/bin/activate && python worker.py'

healthcheck:
	curl https://localhost --insecure

test: start healthcheck stop

# test_infra:
# 	docker run --rm \
# 		--entrypoint '' \
#     -v /var/run/docker.sock:/var/run/docker.sock:ro \
#     -v $(PWD):/code:ro \
#     wernight/docker-compose \
# 		sh -c 'apk update && apk add make curl docker && make test'
