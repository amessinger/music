const fs = require('fs');

const FastBootAppServer = require('fastboot-app-server');

fs.stat('dist', function (err, stats){
  if (err) {
    console.log('No dist folder present');
    return
  }
  if (stats.isDirectory()) {
    let server = new FastBootAppServer({
      distPath: 'dist',
      host: '0.0.0.0',
      port: 8000
    });

    server.start();
  }
});
