# returns the first listed attribute the object has
def get_attr(obj, attr_list):
    if not isinstance(attr_list, list):
        attr_list = [attr_list]
    for attr in attr_list:
        if attr in obj:
            return obj[attr]
    return ''

def get_first(attr):
    if isinstance(attr, list):
        return attr[0]
    else:
        return attr

def get_first_attr(obj, attr_list):
    return get_first(get_attr(obj, attr_list))
