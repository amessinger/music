from os import walk, path
import mutagen
from helpers import get_first_attr
from pymongo import MongoClient

def parse_songs(songs_root, allowed_extensions = ['.flac']):
    songs = []
    for root, dirs, files in walk(songs_root):
        song_files = filter(lambda f : path.splitext(f)[1] in allowed_extensions, files)
        songs += map(lambda f : format_song(path.join(root, f)), song_files)
    return songs

def write_songs(songs, database = 'database'):
    client = MongoClient(database)
    db = client.library
    db.songs.drop()
    db.songs.insert_many(songs)

def format_song(song_path):
    song_tag = mutagen.File(song_path)
    song = {
        'path': song_path,
        'title': get_first_attr(song_tag, 'title'),
        'album': get_first_attr(song_tag, 'album'),
        'artist': get_first_attr(song_tag, ['artist', 'album artist', 'composer']),
        'track': get_first_attr(song_tag, ['track', 'tracknumber']),
        'tracktotal': get_first_attr(song_tag, 'tracktotal')
    }
    return song

write_songs(parse_songs('/media/storage'))
