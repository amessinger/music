const path = require('path');
const express = require('express');
const MongoClient = require('mongodb').MongoClient;

const app = express();

app.listen(8000, function () {
  console.log(`API server listening on port 8000`);
});

app.get('/songs', function (req, res) {
  MongoClient.connect('mongodb://database:27017/library', (err, db)=> {
    db.collection('songs').find({}).toArray((err, songs)=> {
      songs.forEach((song)=> {
        song.id = song._id;
        delete song._id;
        song.path = song.path.replace(/media\/storage/, 'filer')
      });
      res.json(songs);
      db.close();
    });
  });
});
